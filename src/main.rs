//#[macro_use] extern crate log;

use clout::{self, error, info, status, success};

mod progress;
mod remote;
mod s3;
mod store;
mod sync;
mod update;

use store::Store;

use std::fs;
use std::path::Path;
use std::process::ExitCode;
use tokio;

const KSYNC: &'static str = ".kitchensync";
const KSYNCREMOTE: &'static str = ".kitchensync-remote";

type KResult<T> = Result<T, Box<dyn std::error::Error>>;

#[tokio::main]
async fn main() -> ExitCode {
    let args = parse_args();

    let env = env_logger::Env::new()
        .filter("KSYNC_LOG")
        .write_style("KSYNC_LOG_STYLE");
    let mut builder = env_logger::Builder::from_env(env);
    builder.init();

    clout::init()
        .with_verbose(args.get_count("verbose") as u8)
        .with_quiet(args.get_flag("quiet"))
        .done()
        .expect("error setting up clout");

    match dispatch_command(args).await {
        Ok(()) => ExitCode::SUCCESS,
        Err(e) => {
            error!("{}", e);
            ExitCode::FAILURE
        }
    }
}

fn parse_args() -> clap::ArgMatches {
    let args = clap::Command::new("kitchensync")
        .version("0.1")
        .author("Stephen Lee <sphen.lee@gmail.com>")
        .about("Serverless file synchronisation tool")
        .subcommand_required(true)
        .arg(
            clap::Arg::new("verbose")
                .long("verbose")
                .short('v')
                .action(clap::ArgAction::Count)
                .help("Output more logging"),
        )
        .arg(
            clap::Arg::new("quiet")
                .long("quiet")
                .short('q')
                .action(clap::ArgAction::SetTrue)
                .help("Silence all logging"),
        )
        .subcommand(
            clap::Command::new("update")
                .about("Updates the local store")
                .arg(
                    clap::Arg::new("deleted")
                        .long("deleted")
                        .action(clap::ArgAction::SetTrue)
                        .help("check for deleted files"),
                ),
        )
        .subcommand(
            clap::Command::new("sync")
                .about("Perform a synchronisation")
                .arg(
                    clap::Arg::new("target")
                        .help("The remote target to synchronize with")
                        .required(true),
                )
                .arg(
                    clap::Arg::new("push")
                        .long("push")
                        .short('p')
                        .action(clap::ArgAction::SetTrue)
                        .help("Push to the remote store rather than pulling"),
                )
                .arg(
                    clap::Arg::new("dry-run")
                        .long("dry-run")
                        .short('n')
                        .action(clap::ArgAction::SetTrue)
                        .help("Dry run, print actions but do not perform them"),
                )
                .arg(
                    clap::Arg::new("delete")
                        .long("delete")
                        .action(clap::ArgAction::SetTrue)
                        .help("delete files removed from remote target"),
                ),
        )
        .get_matches();

    args
}

async fn dispatch_command(args: clap::ArgMatches) -> KResult<()> {
    match args.subcommand() {
        Some(("update", subargs)) => {
            let opts = UpdateOpts {
                deleted: subargs.get_flag("deleted"),
            };
            do_update(opts)
        }
        Some(("sync", subargs)) => {
            let opts = SyncOpts {
                target: subargs.get_one::<String>("target").unwrap().clone(),
                push: subargs.get_flag("push"),
                dry_run: subargs.get_flag("dry-run"),
                delete: subargs.get_flag("delete"),
            };
            do_sync(opts).await
        }
        _ => panic!("subcommands are supposed to be enforced by clap"),
    }
}

pub struct UpdateOpts {
    deleted: bool,
}

fn do_update(opts: UpdateOpts) -> KResult<()> {
    status!("updating");

    let store = Store::read(KSYNC).unwrap_or_else(|_err| Store::empty());

    info!("getting files");
    let files = update::get_files(".")?;

    info!("looking for changes");
    let updated_store = update::update_store(&opts, store, files)?;

    updated_store.write_to_file(KSYNC)?;

    success!("update successful");

    Ok(())
}

struct SyncOpts {
    target: String,
    push: bool,
    dry_run: bool,
    delete: bool,
}

async fn do_sync(opts: SyncOpts) -> KResult<()> {
    // get the remote ksync file locally
    let mut remote = remote::from_location(&opts.target)?;

    status!(
        "syncing {} {}",
        (if opts.push { "to" } else { "from" }),
        opts.target
    );

    let ksync = Path::new(KSYNC);
    let ksyncremote = Path::new(KSYNCREMOTE);

    info!("get remote store locally");
    let mut got_remote_store = true;
    remote.get(ksync, ksyncremote).await.or_else(|err| {
        match err {
            remote::Error::NotFound(_) if opts.push => {
                got_remote_store = false;
                Ok(())
            },
            err => Err(err),
        }
    })?;

    // {
    //     Ok(()) => {},
    //      if opts.push {
    //         got_remote_store = false;
    //     },
    //     Err(err) => return Err(err)
    // }

    // read both stores
    info!("reading local store");
    let lstore = Store::read(ksync).unwrap_or_else(|_err| Store::empty());
    info!("reading remote store");
    let rstore = Store::read(ksyncremote).unwrap_or_else(|_err| Store::empty());

    //println!("LOCAL {:?}", lstore);
    //println!("REMOTE {:?}", rstore);

    // compare the stores
    info!("comparing stores");
    let (mut actions, removes) = if opts.push {
        sync::get_actions(rstore, lstore) // get_actions takes destination then source
    } else {
        sync::get_actions(lstore, rstore)
    };

    if opts.delete {
        actions.extend(removes);
    }

    if opts.dry_run {
        sync::show_actions(actions);

        info!("removing local copy of remote store");
        fs::remove_file(ksyncremote)?;
    } else {
        info!("performing sync");

        if opts.push {
            sync::perform_push_actions(actions, &mut *remote).await?;

            info!("uploading store to remote");
            remote.put(ksync, ksync).await?;

            if got_remote_store {
                info!("removing local copy of remote store");
                fs::remove_file(ksyncremote)?;
            }
        } else {
            sync::perform_pull_actions(actions, &mut *remote).await?;

            info!("update local store");
            fs::rename(ksyncremote, ksync)?;
        }
    }

    success!("sync successful");

    Ok(())
}
